K��nt�minen:

Ohjelma k��ntyy k��nt�m�ll� Huffman.java, joka l�ytyy src-kansion juuresta.

---

K�ytt�minen:

Ohjelmaa voi k�ytt�� pakkaamiseen tai purkamiseen, haluttu toiminto, tiedosto ja luotavan uuden tiedoston nimet annetaan komentoriviparametrein argumentteina. Syntaksi on:

-pakkaus:
java Huffman pack tiedostonimi uusi_tiedostonimi

-purku:
java Huffman unpack tiedostonimi uusi_tiedostonimi

tiedostonimi: Pakattava/purettava tiedosto
uusi_tiedostonimi: Luotava uusi tiedosto, joka on siis pakattu/purettu tiedosto

tiedostonimi tulee viitata tiedostoon joka on olemassa ja ei ole tyhj�. uusi_tiedostonimi tulee viitata tiedostoon jota EI OLE olemassa.

---

Muuta:

Purkua voi k�ytt�� my�s tiedostoihin, joita ei ole pakattu ohjelmalla, mutta t�ll�in tuloksena on virheilmoitus mik�li tiedosto on liian lyhyt tai tiedosto, jonka sis�lt� on t�ysin merkitykset�nt�.

K�ytt�ohje l�ytyy my�s toteutusdokumentista.