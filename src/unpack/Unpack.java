package unpack;

import datastruct.Node;
import io.BitReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Unpacks a file packaged in Huffman code.
 */
public class Unpack {

	/**
	 * Constructor.
	 */
	public Unpack(String filename, String new_filename) throws IOException {
		BitReader reader = new BitReader(filename);
		Node root = rebuildTree(reader);
		RandomAccessFile out = new RandomAccessFile(new_filename, "rw");
		Node leaf = root;
		if (leaf.getChar() != -1) { // If only one byte
			while (true) {
				try {
					reader.readBit();
					out.write(leaf.getChar());
				} catch (EOFException e) {
					break;
				}
			}
		} else {
			while (true) { // If multiple bytes
				try {
					if (leaf.getChar() != -1) {
						out.write(leaf.getChar());
						leaf = root;
					} else if (reader.readBit()) {
						leaf = leaf.getRight();
					} else {
						leaf = leaf.getLeft();
					}
				} catch (EOFException e) {
					break;
				}
			}
		}
		out.close();
		reader.close();
	}

	/**
	 * Rebuilds a Huffman tree that has been saved at the start of a packed file.
	 *
	 * @param reader the BitReader used to read the file
	 * @return Node root of the rebuilt Huffman tree
	 */
	private Node rebuildTree(BitReader reader) throws IOException {
		if (reader.readBit()) {
			int x = reader.readByte();
			return new Node(x, 0);
		} else {
			Node node = new Node(-1, 0);
			node.setLeft(rebuildTree(reader));
			node.setRight(rebuildTree(reader));
			return node;
		}
	}
}