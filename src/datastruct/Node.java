package datastruct;

/**
 * A binary tree node.
 */
public class Node {
	private int chara;
	private int freq;
	private Node left;
	private Node right;
	
	/**
	 * Constructor.
	 * Assigns the arguments to chara and freq.
	 *
	 * @param chr character of the Node
	 * @param frq frequence of the Node
	 */
	public Node(int chr, int frq) {
		chara = chr;
		freq = frq;
	}

	/**
	 * Returns the character of the Node.           ??? UNUSED ??? DELETE ???
	 * 
	 * @return int character of the Node
	 */
	public int getChar() {
		return chara;
	}

	/**
	 * Returns the frequence of the Node.
	 * 
	 * @return int frequence of the Node
	 */
	public int getFreq() {
		return freq;
	}

	/**
	 * Returns the left child of the Node.
	 *
	 * @return Node left child of the Node
	 */
	public Node getLeft() {
		return left;
	}

	/**
	 * Returns the right child of the Node.
	 *
	 * @return Node right child of the Node
	 */	
	public Node getRight() {
		return right;
	}

	/**
	 * Sets the character of the Node.
	 *
	 * @param chr the new character
	 */
	public void setChar(int chr) {
		chara = chr;
	}

	/**
	 * Sets the frequence of the Node.
	 *
	 * @param frq the new frequence
	 */
	public void setFreq(int frq) {
		freq = frq;
	}

	/**
	 * Sets the left child of the Node.
	 *
	 * @param lft the new left child
	 */
	public void setLeft(Node lft) {
		left = lft;
	}
	
	/**
	 * Sets the child of the Node.
	 *
	 * @param rght the new right child
	 */
	public void setRight(Node rght) {
		right = rght;
	}
}