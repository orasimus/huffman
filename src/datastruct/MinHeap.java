package datastruct;

/**
 * Minimum heap.
 */
public class MinHeap {
	private int length;
	private int heapsize;
	private Node[] data;
	
	/**
	 * Constructor.
	 * Assigns the maximum length from the argument, sets initial heapsize to 0
	 * and creates a single dimension table[length] for the heap
	 * 
	 * @param lngth maximum length for the heap
	 */
	public MinHeap(int lngth) {
		length = lngth + 1; // +1 because table[0] is not in use
		heapsize = 0;
		data = new Node[length];
	}
	
	/**
	 * Inserts a Node into the heap.
	 * 
	 * @param k Node to be inserted into the heap
	 */
	public void insert(Node k) {
		if (heapsize == length) {
			System.out.println("This heap is full.");
		} else {
			heapsize++;
			int i = heapsize;
			while (i > 1 && data[getParent(i)].getFreq() > k.getFreq()) {
				data[i] = data[getParent(i)];
				i = getParent(i);
			}
			data[i] = k;
		}
	}

	/**
	 * Returns the Node with the smallest frequence in the heap.
	 * 
	 * @return Node Node with the smallest frequence from the heap
	 */
	public Node minimum() {
		return data[1];
	}
	
	/**
	 * Removes the Node with the smallest frequence from the heap,
	 * returns it and fixes the heap with heapify().
	 * 
	 * @return Node Node with the smallest frequence from the heap
	 */
	public Node deleteMin() {
		Node min = data[1];
		data[1] = data[heapsize];
		heapsize--;
		heapify(1);
		return min;
	}
	
	/**
	 * Minimum heap decrease key function.
	 * 
	 * @param i
	 * @param k
	 */
	public void decreaseKey(int i, Node k) {
		if (k.getFreq() < data[i].getFreq()) {
				while (i < 1 && data[getParent(i)].getFreq() < k.getFreq()) {
					data[i] = data[getParent(i)];
					i = getParent(i);
				}
				data[i] = k;
		}
	}
	
	/**
	 * Calculates the location of the argument's parent in the heap table.
	 * 
	 * @param i argument for the calculation
	 */
	private int getParent(int i) {
		return i/2;
	}
	
	/**
	 * Calculates the location of the argument's left child in the heap table.
	 * 
	 * @param i argument for the calculation
	 */
	private int getLeft(int i) {
		return 2*i;
	}
	
	/**
	 * Calculates the location of the argument's right child in the heap table.
	 * 
	 * @param i argument for the calculation
	 */
	private int getRight(int i) {
		return 2*i + 1;
	}
	
	/**
	 * Fixes the heap to minimum heap balance
	 * starting (and going downwards) from the index, given as an argument.
	 *
	 * @param i index where to start fixin'
	 */
	private void heapify(int i) {
		int next;
		int l = getLeft(i);
		int r = getRight(i);
		if (l <= heapsize && data[l].getFreq() < data[i].getFreq()) {
			next = l;
		}
		else {
			next = i;
		}
		if (r <= heapsize && data[r].getFreq() < data[next].getFreq()) {
			next = r;
		}
		if (next != i) {
			Node tmp = data[i];
			data[i] = data[next];
			data[next] = tmp;
			heapify(next);
		}
	}
}