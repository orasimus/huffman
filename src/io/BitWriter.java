package io;

import io.BitsAndBytes;
import java.io.*;

/**
 * Writes bits and bytes into a file.
 */
public class BitWriter {
	private RandomAccessFile out;
	private boolean[] bitBuffer = new boolean[8];
	private int bufferindex;

	/**
	 * Constructor.
	 * Opens a file given as an argument.
	 * Writes an int at the start of the file to hold space for information about trashbits.
	 *
	 * @param filename filename of the file to be opened
	 */
	public BitWriter(String filename) throws IOException {
		out = new RandomAccessFile(filename, "rw");
		bufferindex = 0;
	}
	
	/**
	 * Adds argument bit into bitBuffer, unless it's full:
	 * In this case uses writeBuffer() and adds the bit afterwards.
	 * 
	 * @param bit bit to be written
	 */
	public void writeBit(int bit) throws IOException {
		if (bufferindex < 8) {
			bitBuffer[bufferindex] = ((bit & 1) != 0);
			bufferindex++;
		} else {
			writeBuffer();
			bitBuffer[bufferindex] = ((bit & 1) != 0);
			bufferindex++;
		}
	}

	/**
	 * Writes given int as a byte into the file.
	 * Adds bits to bitBuffer until it is full,
	 * then uses writeBuffer() and adds the remaining bits to bitBuffer.
	 * 
	 * @param byt byte to be written
	 */
	public void writeByte(int byt) throws IOException {
		boolean[] bits = BitsAndBytes.byteToBits(byt);
		int byteindex = 0;
		while(bufferindex < 8) {
			bitBuffer[bufferindex] = bits[byteindex];
			bufferindex++;
			byteindex++;
		}
		writeBuffer();
		while(byteindex < 8) {
			bitBuffer[bufferindex] = bits[byteindex];
			bufferindex++;
			byteindex++;
		}
	}

	/**
	 * Fills the bitBuffer with 0s until the byte is complete,
	 * writes the byte and then at the start of the file the amount of trashbits.
	 */
	public void writeTrash() throws IOException {
		int trashbits = 0;
		if (bufferindex != 0) {
			while (bufferindex < 8) {
				bitBuffer[bufferindex] = false;
				trashbits++;
				bufferindex++;
			}
			writeBuffer();
		}
		try {
			out.seek(0);
			out.write(trashbits);
		} catch (IOException e) {
			try {
				out.close();
				throw e;
			} catch (IOException e2) {
				throw e2;
			}
		}
	}

	/**
	 * Closes the RandomAccessFile.
	 */
	public void close() throws IOException {
		out.close();
	}

	/**
	 * Writes the bitBuffer into the file and sets bufferindex back to 0.
	 */
	private void writeBuffer() throws IOException {
		try {
			out.write(BitsAndBytes.bitsToByte(bitBuffer));
			bufferindex = 0;
		} catch (IOException e)	 {
			try {
				out.close();
				throw e;
			} catch (IOException e2) {
				throw e2;
			}
		}
	}

}