package io;

import io.BitsAndBytes;
import java.io.*;

/**
 * Reads a file bit by bit.
 */
public class BitReader {
	private RandomAccessFile in;
	private boolean[] bitBuffer = new boolean[8];
	private int bufferindex;
	private long fp;
	private long length;
	private int trashbits;
	private int bitsread;
	boolean lastByte;
	
	/**
	 * Opens a file given as an argument to a FileInputStream in,
	 * sets bufferindex to 0 and reads the first line to bitBuffer.
	 * If EOF, then closes the file.
	 *
	 * @param filename filename of the file to be opened
	 */
	public BitReader(String filename) throws IOException {
		in = new RandomAccessFile(filename, "r");
		bufferindex = 8;
		fp = 0;
		length = in.length();
		lastByte = false;
		trashbits = readByte();
		bitsread = 0;
	}
	
	/**
	 * Returns bitBuffer[bufferindex],
	 * if bufferindex is 8, then reads another byte into bitBuffer.
	 *
	 * @return boolean the read bit as an int
	 */
	public boolean readBit() throws IOException {
		if (lastByte && (8 - trashbits) == bitsread) {
			throw new EOFException("Only trash bits left.");
		}
		int b;
		try {
			if (bufferindex < 8) {
				if (lastByte) {
					bitsread++;
				}
				bufferindex++;
				return bitBuffer[(bufferindex - 1)];
			} else if ((b = in.read()) != -1) {
				fp = in.getFilePointer();
				if (fp == length) {
					lastByte = true;
					bitsread++;
				}
				bitBuffer = BitsAndBytes.byteToBits(b);
				bufferindex = 0;
				bufferindex++;
				return bitBuffer[(bufferindex - 1)];
			} else {
				throw new EOFException("EOF. Perhaps you attempted to unpack a file that wasn't packed with this program.");
			}
		} catch (IOException e) {
			try {
				in.close();
				throw e;
			} catch (IOException e2) {
				throw e2;
			}
		}
	}
	
	/**
	 * Reads a byte and returns it as an int.
	 *
	 * @return int the read byte as an int
	 */
	public int readByte() throws IOException {
		boolean[] bits = new boolean[8];
		for (int i = 0; i < 8; i++) {
			bits[i] = readBit();
		}
		return BitsAndBytes.bitsToByte(bits);
	}

	/**
	 * Closes the RandomAccessFile.
	 */
	public void close() throws IOException {
		in.close();
	}
}