package io;

/**
 * Functions that constructs bytes from bits and separates bytes to bits.
 */
public class BitsAndBytes {
	/**
	 * Turns the boolean[] into an int.
	 *
	 * @param bits 8 booleans representing bits (false = 0, true = 1)
	 * @return int boolean[] bits turned into an int
	 */
	public static int bitsToByte(boolean[] bits) {
		int byt = 0;
		for (int i = 0; i < 8; i++) {
			if (bits[i]) byt += (1 << (7-i));
		}
		return byt;
	}
	
	/**
	 * Separates argument int to bits.
	 *
	 * @param byt int to be separated into bits
	 * @return boolean[] bits in a boolean[]
	 */
	public static boolean[] byteToBits(int byt) {
		boolean[] bits = new boolean[8];
		for (int i=0; i < 8; i++) {
			bits[i] = ( (byt & (1 << (7-i)) ) != 0 );
		}
		return bits;
	}
}