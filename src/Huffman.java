import pack.Pack;
import unpack.Unpack;

import java.io.File;
import java.io.IOException;

/**
 * Packages or unpacks a file in Huffman code.
 * Uses classes Pack and Unpack to achieve this.
 */
public class Huffman {
	public static void main(String[] args) {
		if (args.length == 3 && ((new File(args[1])).length()) != 0 && !((new File(args[2])).exists())) {
			if (args[0].equals("pack")) {
				System.out.println("Packing file " + args[1] + " to file " + args[2] + "...");
				try {
					Pack pack = new Pack(args[1], args[2]);
					System.out.println("Done!");
				} catch (IOException e) {
					System.out.println("Error. Message:");
					System.out.println(e.getMessage());
				}
			} else if (args[0].equals("unpack")) {
				System.out.println("Unpacking file " + args[1] + " to file " + args[2] + "...");
				try {
					Unpack unpack = new Unpack(args[1], args[2]);
					System.out.println("Done!");
				} catch (IOException e) {
					System.out.println("Error. Message:");
					System.out.println(e.getMessage());
				}
			}
		} else if (args.length != 3) {
			System.out.println("Invalid amount of arguments (should be 3).");
			System.out.println("Use syntax: 'java Huffman pack/unpack target_filename destination_filename'");
		} else {
			System.out.println("Invalid file.");
			System.out.println("Target file has to be a file that exists and isn't empty.");
			System.out.println("Destination file has to be a file that DOESN'T exist.");
		}
	}
}