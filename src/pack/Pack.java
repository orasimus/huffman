package pack;

import datastruct.MinHeap;
import datastruct.Node;
import io.BitWriter;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Packages a file in Huffman code.
 */
public class Pack {
	String[] codes = new String[256];
	
	/**
	 * Constructor.
	 * Packs the file given in the argument using Huffman coding.
	 *
	 * @param fn file to be packed
	 * @param newfn destination filename
	 */
	public Pack(String fn, String newfn) throws IOException {
		Node root;
		Node[] nodes = createNodes(readBytes(fn));
		if (nodes.length == 1) {
			root = nodes[0];
			codes[root.getChar()] = "0";
		} else {
			root = huffman(nodes);
			createCodes(root, "");
		}
		encodeFile(fn, newfn, root);
	}
	
	/**
	 * Reads a file in bytes.
	 *
	 * @param fn filename to be read
	 * @return Node[] different bytes & their occurences
	 */
	private int[] readBytes(String fn) throws IOException {
		RandomAccessFile in = new RandomAccessFile(fn, "r");
		int[] bytes = new int[256];
		int b;
		while ((b = in.read()) != -1) {
			bytes[b]++;
		}
		in.close();
		return bytes;
	}

	/**
	 * Translates an int[] of different bytes occurences
	 * to a Node[] of the same information.
	 *
	 * @param bytes occurrences of different bytes
	 * @return Node[] different bytes translated to Nodes
	 */
	private Node[] createNodes(int[] bytes) {
		int counter = 0;
		for (int i = 0; i < bytes.length; i++) {
			if (bytes[i] != 0) {
				counter++;
			}
		}	
		Node[] nodes = new Node[counter];
		int j = 0;
		for (int i = 0; i < bytes.length; i++) {
			if (bytes[i] != 0) {
				nodes[j] = new Node(i, bytes[i]);
				j++;
			}
		}
		return nodes;
	}

	/**
	 * Creates a Huffman tree from a set of Nodes.
	 *
	 * @param nodes set of Nodes
	 * @return Node root of the Huffman tree
	 */
	private Node huffman(Node[] nodes) {
		int n = nodes.length;
		MinHeap heap = new MinHeap(n);
		for (int i = 0; i < n; i++) {
			heap.insert(nodes[i]);
		}
		Node x;
		Node y;
		for (int i = 1; i < n; i++) {
			Node z = new Node(0, 0);
			x = heap.deleteMin();
			z.setLeft(x);
			y = heap.deleteMin();
			z.setRight(y);
			z.setFreq(x.getFreq() + y.getFreq());
			heap.insert(z);
		}
		return heap.deleteMin();
	}

	/**
	 * Translates a Huffman tree into bit codes.
	 *
     * @param node root of the tree to be translated
	 * @param prefix current code while travelsing the tree
     */
	private void createCodes(Node node, String prefix) {
		if ((node.getLeft() == null) && (node.getRight() == null)) {
			codes[node.getChar()] = prefix;
		} else {
			createCodes(node.getLeft(), (prefix + "0"));
			createCodes(node.getRight(), (prefix + "1"));
		}
	}

	/**
	 * Codes a file using the built Huffman code.
	 *
	 * @param fn the file to be coded
	 * @param newfn destination filename
	 * @param root root of the Huffman tree
	 */
	private void encodeFile(String fn, String newfn, Node root) throws IOException {
		RandomAccessFile in = new RandomAccessFile(fn, "rw");		
		BitWriter writer = new BitWriter(newfn);
		int b;
		writer.writeByte(0); // Room for the trashbits
		writeTree(root, writer);
		while ((b = in.read()) != -1) {
			for (int i = 0; i < codes[b].length(); i++) {
				char c = codes[b].charAt(i);
				if (c == '0') {
					writer.writeBit(0);
				} else {
					writer.writeBit(1);
				}
			}
		}
		in.close();
		writer.writeTrash();
		writer.close();
	}
	
	/**
	 * Writes the Huffman tree at the start of the encoded file.
	 *
	 * @param writer BitWriter for writing bits and bytes
	 */
	private void writeTree(Node node, BitWriter writer) throws IOException {
		if (node != null) {
			if (node.getLeft() == null && node.getRight() == null) {	
				writer.writeBit(1);
				writer.writeByte(node.getChar());
			} else {
				writer.writeBit(0);
				writeTree(node.getLeft(), writer);
				writeTree(node.getRight(), writer);
			}
		}
	}
}